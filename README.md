# Neovimux

A config to replicate tmux workflow, without tmux.

Alpha state / under construction. Expect breaking changes.

## features
- vim-like mappings for opening terminal splits
- navigate across buffers and terminals seamlessly
- configurable map leader key
- telescope picker for running terminals

## todo
- [ ] naming terminals
- [ ] make mappings sets configurable/optional
- [ ] sessions
  - session save/restore
  - telescope session picker
  - keep background terminals running, not in the buflist? (seems so
  impossible)
- [ ] use tmux shortcuts within nvim terminal

## known issues
- the picker sometimes opens the current file rather than the underlying
terminal, for some reason

## philosophy

### Tmux is a very likable terminal tool.

It helps spawning multiple terminal windows in a sole one, using different
sessions, which can hold several windows (tabs in vim), which can show several
panels (splits in vim).

It's able to easily copy and paste text, to organize windows, to start in the
background for instant loading times, to keep tasks running in hidden windows,
to save and load sessions with their layouts and terminal jobs, across
restarts, be used over SSH and so on.

It can go anywhere from making your life in the terminal more comfortable to
making you feel like a terminal wizard. 

So, why think of dropping it? Well, I don't.

### why

### tmux rough spots

Tmux being a wrapper around the terminal, it requires a 'leader' key for its
key mappings. The problem is that this 'leader' is then not available in vim
and other applications, and needs to be typed twice to be sent forward.

Also, navigation between vim splits and Tmux panes can be confusing as they use
different sets of mappings. One can use
[vim-tmux-navigator](https://github.com/christoomey/vim-tmux-navigator) to
harmonize things. It does not cut it in my case since I already mapped every
combination that would makes sense for this task (both *ctrl-hjkl* and
*meta-hjkl*).

By setting the leader key vim, we get more control on our mappings.
Namely, we can set the leader in 'normal' and 'terminal' mode only (that's default).
The leader is then free to be used in other modes.

Finally, the project seems fun and easy to get spin up. More on that
later.

But there might be another reason.

### Neovide

Neovide feels so great. It just makes the UI feels a lot more comfortable.
Shadows and animations, I never thought I'd enjoy, but in the end it helps me
to keep track of navigation. Also, it's so smooth, very satisfying, I must
confess.

While I love the terminal, I feel like a GUI could be the way to go nowadays,
at least when not connected to a server - which is most of my work these days.

### vision

For now, I just want to see if I could use Neovim the same way I use Tmux,
since it can spawn terminal emulators as well. This could change the way I use
vim and the terminal.

The initial steps are merely a bit of mappings and auto-commands to kinda mimic
Tmux's behaviours. This task is quite approachable, and constitute the first
commit.

Now, it's about implementing a way to save, load, remove sessions, and reload
them without losing information in the terminal buffers.

