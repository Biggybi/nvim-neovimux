local M = {}

if package.loaded["telescope"] == nil then
	return M
end

local actions = require("telescope.actions")
local actions_state = require("telescope.actions.state")
local entry_display = require("telescope.pickers.entry_display")
local previewers = require("telescope.previewers")

local pickers = require("telescope.pickers")
local finders = require("telescope.finders")
local config = require("telescope.config").values

function M.find_terminal_buffers()
	local bufnrs = vim.tbl_filter(function(b)
		return vim.api.nvim_get_option_value("buftype", { buf = b }) == "terminal"
	end, vim.api.nvim_list_bufs())
	if #bufnrs == 0 then
		return { { bufnr = 0, name = "No terminal buffers" } }
	end
	local entries = {}
	for _, b in ipairs(bufnrs) do
		table.insert(entries, {
			bufnr = b,
			name = vim.api.nvim_buf_get_name(b),
		})
	end
	return entries
end

local terminal_displayer = entry_display.create({
	separator = " ",
	items = {
		{ width = 4 },
		{ remaining = true },
	},
})

local function terminal_usage_preview(self)
	local usage = {
		"new terminal",
		"",
		"^M  ->  current",
		"^T  ->  tab",
		"^X  ->  split",
		"^V  ->  vert split",
	}

	local usage_width = math.max(unpack(vim.tbl_map(function(line)
		return #line
	end, usage)))
	local winwidth = vim.api.nvim_win_get_width(self.state.winid)
	local margin_left = (" "):rep(((winwidth - usage_width) / 2) - 1)

	local winheight = vim.api.nvim_win_get_height(self.state.winid)
	local margin_top = ((winheight - #usage) / 2)

	-- add left margin
	local lines = vim.tbl_map(function(line)
		return margin_left .. line
	end, usage)
	-- add top margin
	for _ = 1, margin_top do
		table.insert(lines, 1, "")
	end

	return lines
end

local function list_terminal_previewer()
	return previewers.new_buffer_previewer({
		get_buffer_by_name = function(_, entry)
			return entry.name
		end,
		define_preview = function(self, entry)
			local lines = vim.api.nvim_buf_get_lines(entry.value.bufnr, 0, -1, false)
			if entry.value.bufnr == 0 then
				lines = terminal_usage_preview(self)
			end
			vim.api.nvim_buf_set_lines(self.state.bufnr, 0, 0, true, lines)
		end,
	})
end

local function open_term(bufnr, layout)
	local options = require("neovimux.config").options
	require("neovimux.actions.open_term").create_window(layout)

	if bufnr ~= 0 or layout == "current" then
		vim.api.nvim_set_current_buf(bufnr)
	end

	if bufnr == 0 then
		vim.cmd("term")
		if options.cursor.start_insert then
			vim.cmd("startinsert")
		end
	end
end

local function list_terminal_keymaps(prompt_bufnr, map)
	actions.select_default:replace(function()
		local selection = actions_state.get_selected_entry()
		actions.close(prompt_bufnr)
		open_term(selection.value.bufnr, "current")
	end)

	map({ "i", "n" }, "<C-v>", function()
		local selection = actions_state.get_selected_entry()
		actions.close(prompt_bufnr)
		open_term(selection.value.bufnr, "vertical")
	end)

	map({ "i", "n" }, "<C-x>", function()
		local selection = actions_state.get_selected_entry()
		actions.close(prompt_bufnr)
		open_term(selection.value.bufnr, "horizontal")
	end)

	map({ "i", "n" }, "<C-t>", function()
		local selection = actions_state.get_selected_entry()
		actions.close(prompt_bufnr)
		open_term(selection.value.bufnr, "tab")
	end)

	map({ "i", "n" }, "<C-.>", function()
		local selection = actions_state.get_selected_entry()
		actions.close(prompt_bufnr)
		open_term(selection.value.bufnr, "float")
	end)

	return true
end

local function terminal_telescope_finder()
	return finders.new_table({
		results = M.find_terminal_buffers(),
		entry_maker = function(entry)
			return {
				value = entry,
				display = function()
					return terminal_displayer({
						{ entry.bufnr, "TelescopeResultsNumber" },
						entry.name,
					})
				end,
				ordinal = entry.name .. ":" .. entry.bufnr,
			}
		end,
	})
end

M.list_terminals = function(opts)
	pickers
		.new(opts, {
			finder = terminal_telescope_finder(),
			sorter = config.generic_sorter(opts),
			previewer = list_terminal_previewer(),
			attach_mappings = function(prompt_bufnr, map)
				return list_terminal_keymaps(prompt_bufnr, map)
			end,
		})
		:find()
end

return M
