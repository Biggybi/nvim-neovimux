local M = {}

function M.is_tmux()
	return vim.env.TMUX ~= nil and vim.env.TERM:match("^\\(screen|tmux)")
end

return M
