local M = {}

local options = require("neovimux.config").options
local term_opts = options.options
local cursor = options.cursor

local function startinsert()
	vim.cmd("startinsert")
end

local function terminal_options()
	if vim.bo.buftype ~= "terminal" then
		return
	end
	vim.o.number = term_opts.number
	vim.o.relativenumber = term_opts.relativenumber
end

function M.set_terminal_autocmds()
	vim.api.nvim_create_autocmd({ "TermOpen" }, {
		group = vim.api.nvim_create_augroup("NeovimuxTerminalOptionsOpen", {}),
		desc = "Neovimux - term options + mode",
		pattern = "*",
		callback = function()
			terminal_options()
			if cursor.start_insert then
				startinsert()
			end
		end,
	})

	vim.api.nvim_create_autocmd({ "BufEnter" }, {
		group = vim.api.nvim_create_augroup("NeovimuxTermEnter", {}),
		desc = "Neovimux - term options + mode",
		pattern = "*",
		callback = function()
			if vim.bo.buftype ~= "terminal" then
				return
			end
			terminal_options()
			if cursor.force_insert then
				startinsert()
				return
			end
			if vim.b.neovimux_term_moves and vim.b.neovimux_term_moves >= 0 then
				startinsert()
			end
		end,
	})

	vim.api.nvim_create_autocmd({ "CursorMoved" }, {
		group = vim.api.nvim_create_augroup("NeovimuxTermInsertLeave", {}),
		desc = "Neovimux - terminal insert leave",
		pattern = "*",
		callback = function()
			if vim.bo.buftype ~= "terminal" then
				return
			end
			vim.b.neovimux_term_moves = (vim.b.neovimux_term_moves or 0) - 1
		end,
	})

	vim.api.nvim_create_autocmd({ "TermEnter" }, {
		group = vim.api.nvim_create_augroup("TerminalTermInsertEnter", {}),
		desc = "Neovimux - terminal insert enter",
		pattern = "*",
		callback = function()
			if vim.bo.buftype ~= "terminal" then
				return
			end
			vim.b.neovimux_term_moves = 1
		end,
	})
end

return M
