local M = {}

---@class TerminalCursor
---@field auto_restore boolean
---@field start_insert boolean
---@field force_insert boolean

---@class BufferOptions
---@field number boolean
---@field relativenumber boolean

---@class TerminalShell
---@field prompt string

---@class TerminalKeys
---@field termleader string
---@field register_whichkey boolean

---@class TerminalLayoutProps
---@field width? number
---@field height? number
---@field max_width? number | fun(): number
---@field max_height? number | fun(): number

---@alias TerminalLayout 'current' | 'vertical' | 'horizontal' | 'tab' | 'float'

---@class TerminalTelescope
---@field layout_strategy string

---@class TerminalToggle
---@field layout TerminalLayout

---@class TerminalTmuxLayout
---@field width? number
---@field height? number

---@class TerminalTmux
---@field layout TerminalTmuxLayout
---@field hints boolean

---@class TerminalIconsPicker
---@field active_session string
---@field inactive_session string

---@class TerminalIcons
---@field picker TerminalIconsPicker

---@class Terminal
---@field pre_save? fun()
---@field cursor TerminalCursor
---@field icons TerminalIcons
---@field options BufferOptions
---@field shell TerminalShell
---@field keys TerminalKeys
---@field skip_tmux boolean
---@field layouts table<TerminalLayout, TerminalLayoutProps>
---@field telescope TerminalTelescope
---@field toggle TerminalToggle
---@field tmux TerminalTmux

---@type Terminal
local defaults = {
	-- dir = vim.fn.expand(vim.fn.stdpath("state") .. "/sessions/"), -- directory where session files are saved
	cursor = {
		auto_restore = true, -- auto restore terminal insert mode
		start_insert = true, -- open new terminals in insert mode
		force_insert = false, -- force insert mode when entering terminal
	},
	icons = {
		picker = { active_session = "", inactive_session = "" },
	},
	shell = {
		prompt = vim.fn.hostname() .. "\\%", -- terminal prompt, defaults to zsh's default
	},
	options = {
		number = false, -- set number
		relativenumber = false, -- set relativenumber
	},
	keys = {
		termleader = "<C-Space>",
		register_whichkey = true, -- register mappings with which-key
	},
	layouts = {
		vertical = {
			width = 80,
		},
		horizontal = {
			height = 15,
		},
		float = {
			width = 100,
			height = 25,
			max_width = function()
				return vim.o.columns * 0.8
			end,
			max_height = function()
				return vim.o.lines * 0.8
			end,
		},
	},
	telescope = {
		layout_strategy = "center",
	},
	toggle = {
		layout = "float",
	},
	skip_tmux = true, -- don't act on tmux
	tmux = {
		layout = {
			width = 0.5,
			height = nil, -- adaptive by default
		},
		hints = false,
	},
}

---@type Terminal
M.options = defaults

function M.setup(opts)
	M.options = vim.tbl_deep_extend("force", {}, defaults, opts or {})
end

return M
