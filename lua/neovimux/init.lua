local config = require("neovimux.config")
local utils = require("neovimux.utils")
local M = {}

---@param opts Terminal
function M.setup(opts)
	if utils.is_tmux() and opts.skip_tmux then
		return
	end
	vim.keymap.set("n", "<C-Space>o", "<cmd>wincmd gt<CR>")
	config.setup(opts)
	require("neovimux.keymaps").create_keymaps()
	require("neovimux.terminal").set_terminal_autocmds()
end

return M
