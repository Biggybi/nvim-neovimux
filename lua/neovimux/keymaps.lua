M = {}

local config = require("neovimux.config")
local termleader = config.options.keys.termleader

-- create 2 mappings
--   <termleader>lhs
--   <termleader><c-lhs>
---@param modes string[]
---@param lhs string
---@param rhs string | function
---@param opts table | nil
local function double_map(modes, lhs, rhs, opts)
	local r = {
		n = rhs,
		t = type(rhs) == "string" and ("<C-\\><C-n>" .. rhs) or function()
			-- vim.api.nvim_feedkeys("<C-\\><C-n>", "n", true)
			vim.cmd("stopinsert")
			rhs()
		end,
	}

	local function ctrl_map(mode)
		if #lhs ~= 1 then
			return
		end
		-- don't remap <ctrl-UpperCaseLetter>
		if lhs:match("[A-Z]") then
			return
		end
		-- don't remap <ctr-l>special_key in terminal mode
		if not vim.fn.has("gui_running") and not lhs:match("[a-z]") then
			return
		end

		local ctrl_lhs = termleader .. "<c-" .. lhs .. ">"
		if vim.fn.maparg(ctrl_lhs, mode) ~= "" then
			return
		end
		-- vim.print("ctrl_lhs: " .. ctrl_lhs)
		vim.keymap.set(mode, ctrl_lhs, r[mode], opts)
	end

	local function regular_map(mode)
		local regular_lhs = termleader .. lhs
		if vim.fn.maparg(regular_lhs, mode) ~= "" then
			return
		end
		-- vim.print("regular_lhs: " .. regular_lhs)
		vim.keymap.set(mode, regular_lhs, r[mode], opts)
	end

	for _, mode in pairs(modes) do
		regular_map(mode)
		ctrl_map(mode)
	end
end

-- recreate wincmd maps for terminal with termleader
function M.alias_wincmd()
	local wincmds = {
		{ cmd = "o", wincmd = "gt", desc = "Left window" },
		{ cmd = "i", wincmd = "gT", desc = "Left window" },
		{ cmd = "z", wincmd = "o", desc = "Zoom window (!can't undo)" },
	}
	for _, wincmd in pairs(wincmds) do
		double_map({ "n", "t" }, wincmd.cmd, "<C-w>" .. wincmd.wincmd, { desc = wincmd.desc })
	end
end

-- recreate wincmd maps for terminal with termleader
function M.remap_wincmd()
	local wincmds = {
		{ cmd = "h", desc = "Left window" },
		{ cmd = "j", desc = "Bellow window" },
		{ cmd = "k", desc = "Above window" },
		{ cmd = "l", desc = "Right window" },
		{ cmd = "H", desc = "Move window to left" },
		{ cmd = "J", desc = "Move window to bottom" },
		{ cmd = "K", desc = "Move window to top" },
		{ cmd = "L", desc = "Move window to right" },
		{ cmd = "w", desc = "Next window" },
		{ cmd = "W", desc = "Previous window" },
		{ cmd = "gt", desc = "Next tab" },
		{ cmd = "gT", desc = "Previous tab" },
		{ cmd = "b", desc = "Bottom-left window" },
		{ cmd = "t", desc = "Top-left window" },
		{ cmd = "p", desc = "Previous window" },
		{ cmd = "P", desc = "Preview window" },
		{ cmd = "r", desc = "Rotate windows downwards" },
		{ cmd = "R", desc = "Rotate windows upwards" },
		{ cmd = "x", desc = "Swap window" },
		{ cmd = "T", desc = "Move window to new tab" },
		{ cmd = "c", desc = "Close window" },
		{ cmd = "o", desc = "Close other windows" },
		{ cmd = "q", desc = "Quit window" },
		{ cmd = "=", desc = "Equalize window sizes" },
		{ cmd = "-", desc = "Decrease window size" },
		{ cmd = "+", desc = "Increase window size" },
		{ cmd = "<", desc = "Decrease window size" },
		{ cmd = ">", desc = "Increase window size" },
		{ cmd = "|", desc = "Maximize window" },
	}
	for _, wincmd in pairs(wincmds) do
		double_map({ "n", "t" }, wincmd.cmd, "<C-w>" .. wincmd.cmd, { desc = wincmd.desc })
	end
end

function M.core_keymaps()
	-- escape terminal mode
	double_map({ "t" }, "<c-[>", function()
		vim.b.neovimux_term_moves = -10
	end, { desc = "Exit terminal mode" })
	double_map({ "n", "t" }, termleader, "<C-^>", { desc = "Alternate file" })

	-- double_map({ "n", "t" }, "v", "<Cmd> vnew term://zsh <CR>", { desc = "New vertical terminal" })
	-- double_map({ "n", "t" }, "s", "<Cmd> split term://zsh <CR>", { desc = "New New horizontal terminal" })
	-- double_map({ "n", "t" }, "t", "<Cmd> tabnew term://zsh <CR>", { desc = "New tab terminal" })

	double_map({ "n", "t" }, ".", require("neovimux.actions.open_term").toggle_term, { desc = "Toggle terminal" })

	local open_term = require("neovimux.actions.open_term")
	local tmux = require("neovimux.actions.tmux")
	double_map({ "n", "t" }, "f", function()
		require("neovimux.telescope_tmux_sessions").list_tmux_sessions()
	end)
	double_map({ "n", "t" }, "s", open_term.create_term_horizontal, { desc = "New horizontal terminal" })
	double_map({ "n", "t" }, "v", open_term.create_term_vertical, { desc = "New vertical terminal" })
	double_map({ "n", "t" }, "t", open_term.create_term_tab, { desc = "New tab terminal" })
	double_map({ "n", "t" }, "n", open_term.create_term_current, { desc = "New terminal" })

	double_map({ "n", "t" }, "<C-;>", ":", { desc = "Open command-line" })
	double_map({ "n", "t" }, ";", ":", { desc = "Open command-line" })
	double_map({ "n", "t" }, ":", ":", { desc = "Open command-line" })
	double_map(
		{ "n", "t" },
		"d",
		"<Cmd>lua require('neovimux.telescope').list_terminals()<CR>",
		{ desc = "list terminals" }
	)
end

function M.create_keymaps()
	M.core_keymaps()
	M.alias_wincmd()
	M.remap_wincmd()
end

return M
