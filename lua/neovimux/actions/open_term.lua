local M = {}

local options = require("neovimux.config").options
-- function M.create_term()
-- 	local options = require("neovimux.config").options
-- 	local layout = options.toggle.layout
-- 	if layout == "vertical" then
-- 		vim.cmd(options.layouts.vertical.size .. "vnew")
-- 	elseif layout == "tab" then
-- 		vim.cmd("tabnew")
-- 	elseif layout == "float" then
-- 		vim.api.nvim_open_win(0, true, {
-- 			relative = "editor",
-- 			width = 80,
-- 			height = 15,
-- 			row = 10,
-- 			col = 10,
-- 			style = "minimal",
-- 		})
-- 	else
-- 		vim.cmd(options.layouts.horizontal.size .. "new")
-- 	end
-- end

local function create_win_horizontal()
	vim.cmd(options.layouts.horizontal.height .. "new")
end

local function create_win_vertical()
	vim.cmd(options.layouts.vertical.width .. "vnew")
end

local function create_win_tab()
	vim.cmd("tabnew")
end

local function create_current_win()
	vim.cmd("enew")
end

local function create_win_float()
	local max_width = type(options.layouts.float.max_width) == "function"
			and math.floor(options.layouts.float.max_width())
		or options.layouts.float.max_width

	local max_height = type(options.layouts.float.max_height) == "function"
			and math.floor(options.layouts.float.max_height())
		or options.layouts.float.max_height

	---@cast max_width number
	---@cast max_height number
	local width = math.min(options.layouts.float.width, max_width)
	local height = math.min(options.layouts.float.height, max_height)

	vim.api.nvim_open_win(0, true, {
		relative = "editor",
		width = width,
		height = height,
		row = (vim.o.lines - height) / 2,
		col = (vim.o.columns - width) / 2,
		style = "minimal",
	})
end

---@param layout? TerminalLayout
function M.create_window(layout)
	local layouts = {
		horizontal = create_win_horizontal,
		vertical = create_win_vertical,
		tab = create_win_tab,
		float = create_win_float,
		current = create_current_win,
	}

	layouts[layout or options.toggle.layout]()
end

---@param layout? TerminalLayout
function M.create_term(layout)
	M.create_window(layout)
	vim.cmd("term")
end

function M.create_term_horizontal()
	M.create_term("horizontal")
end

function M.create_term_vertical()
	M.create_term("vertical")
end

function M.create_term_tab()
	M.create_term("tab")
end

function M.create_term_float()
	M.create_term("float")
end

function M.create_term_current()
	M.create_term("current")
end

function M.get_tab_toggle_term_win()
	local winids = vim.api.nvim_tabpage_list_wins(0)
	if winids == {} then
		return
	end
	for _, winid in pairs(winids) do
		if vim.b[vim.api.nvim_win_get_buf(winid)]["toggle_term"] then
			return winid
		end
	end
end

function M.get_toggle_term_bufnr()
	if package.loaded["telescope"] == nil then
		return
	end
	local terminal = require("neovimux.telescope").find_terminal_buffers()
	-- pick terminal that holds the 'toggle_term' buffer-local option
	for _, term in pairs(terminal) do
		if vim.b[term.bufnr].toggle_term then
			return term.bufnr
		end
	end
end

function M.toggle_existing(tab_term_win)
	if vim.api.nvim_get_current_win() == tab_term_win then
		vim.api.nvim_win_close(tab_term_win, false)
		return
	end
	vim.api.nvim_set_current_win(tab_term_win)
end

function M.toggle_new_window()
	M.create_window()
	local toggle_term_bufnr = M.get_toggle_term_bufnr()
	if toggle_term_bufnr then
		vim.api.nvim_set_current_buf(toggle_term_bufnr)
	else
		vim.cmd("term")
		vim.b.toggle_term = true
	end
end

function M.toggle_term()
	local tab_term_win = M.get_tab_toggle_term_win()
	if tab_term_win then
		M.toggle_existing(tab_term_win)
		return
	end

	M.toggle_new_window()
end

return M
