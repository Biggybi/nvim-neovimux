local M = {}

function M.open_session(session_name)
	vim.system({ "tmux", "switch-client", "-t", session_name })
end

function M.new_session(session_name)
	vim.system({ "tmux", "new-session", "-d", "-s", session_name })
end

function M.kill_session(session_name)
	vim.system({ "tmux", "kill-session", "-t", session_name })
end

function M.rename_session(session_name, new_name)
	vim.system({ "tmux", "rename-session", "-t", session_name, new_name })
end

function M.change_base_dir_session(session_name, directory)
	vim.system({ "tmux", "attach", "-c", directory, "-t", session_name })
end

function M.tmux_count_sessions()
	local count = 0
	vim.system({ "tmux", "list-sessions" }, { text = true }, function(obj)
		count = #vim.split(obj.stdout, "\n")
	end):wait()
	return count
end

return M
