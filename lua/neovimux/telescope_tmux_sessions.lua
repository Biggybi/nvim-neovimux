local M = {}

local function get_displayer()
	local entry_display = require("telescope.pickers.entry_display")
	local displayer = entry_display.create({
		separator = " ",
		items = {
			{ width = 1 },
			{ width = 15 },
			{ remaining = true },
		},
	})
	return displayer
end

local function get_current_tmux_session_name()
	local current_session
	vim.system({ "tmux", "display-message", "-p", "#S" }, { text = false }, function(obj)
		current_session = obj.stdout
		if not current_session then
			return
		end
		current_session = current_session:gsub("\n", "")
	end):wait()
	return current_session
end

function M.get_tmux_sessions()
	local sessions = {}
	local session_name = get_current_tmux_session_name()
	local current_session = {}

	local options = require("neovimux.config").options
	local on_exit = function(obj)
		local session_strings = vim.split(obj.stdout, "\n", { trimempty = true })
		for _, session_string in ipairs(session_strings) do
			local s = vim.split(session_string, " ")
			if s[2] ~= session_name then
				if s then
					table.insert(
						sessions,
						{ time = s[1], name = s[2], path = s[3], icon = options.icons.picker.inactive_session }
					)
					table.sort(sessions, function(a, b)
						if a.time == "" then
							return false
						end
						if b.time == "" then
							return true
						end
						return tonumber(a.time) > tonumber(b.time)
					end)
				end
			else
				current_session = { time = s[1], name = s[2], path = s[3], icon = options.icons.picker.active_session }
			end
		end
		-- add current session at the end
		table.insert(sessions, current_session)
	end

	vim.system(
		{ "tmux", "list-sessions", "-F", "#{session_last_attached} #S #{session_path}" },
		{ text = true },
		on_exit
	)
		:wait()
	return sessions
end

local function tmux_session_finder()
	local finders = require("telescope.finders")
	return finders.new_table({
		results = M.get_tmux_sessions(),
		entry_maker = function(entry)
			return {
				value = entry,
				display = function()
					return get_displayer()({
						-- { entry.icon, "TelescopeResultsClass" },
						{ entry.icon, "TelescopeResultsComment" },
						{ entry.name, "TelescopeResultsNumber" },
						entry.path,
					})
				end,
				ordinal = entry.name .. ":" .. entry.path,
			}
		end,
	})
end

local function keymaps(prompt_bufnr, map)
	local tmux = require("neovimux.actions.tmux")
	local actions = require("telescope.actions")
	local actions_state = require("telescope.actions.state")

	actions.select_default:replace(function()
		local selection = actions_state.get_selected_entry()
		if
			actions_state.get_selected_entry()
			and actions_state.get_selected_entry().value.name == get_current_tmux_session_name()
		then
			return
		end
		if selection then
			actions.close(prompt_bufnr)
			tmux.open_session(selection.value.name)
		end
	end)

	map("i", "<C-o>", function()
		local new_session_name = actions_state.get_current_picker(prompt_bufnr).sorter._discard_state.prompt
		local sessions = M.get_tmux_sessions()
		if new_session_name == "" then
			new_session_name = vim.fn.input("Create new session: ")
			if new_session_name == "" then
				return
			end
		end
		for _, session in ipairs(sessions) do
			if session.name == new_session_name then
				return
			end
		end
		actions.close(prompt_bufnr)
		tmux.new_session(new_session_name)
		tmux.open_session(new_session_name)
	end)

	map("i", "<M-d>", function()
		local selection = actions_state.get_selected_entry()
		tmux.kill_session(selection.value.name)
		local current_picker = actions_state.get_current_picker(prompt_bufnr)
		current_picker:delete_selection(function()
			return true
		end)
	end)

	map("i", "<c-r><c-n>", function()
		local selection = actions_state.get_selected_entry()
		if not selection then
			selection = { value = { name = get_current_tmux_session_name() } }
		end
		local name = vim.fn.input("Rename session:", selection.value.name)
		tmux.rename_session(selection.value.name, name)
		local current_picker = actions_state.get_current_picker(prompt_bufnr)
		current_picker:refresh(tmux_session_finder())
		-- TODO: select renamed session back
	end)

	map("i", "<C-d>", function()
		local selection = actions_state.get_selected_entry()
		if not selection then
			selection = { value = { name = get_current_tmux_session_name() } }
		end
		local dir = vim.fn.input("Change base directory to: ", vim.fn.getcwd())
		if not dir or not vim.fn.isdirectory(dir) then
			vim.notify(string.format("Cannot session: not a directory '%s'", dir), vim.log.levels.WARN)
			return
		end
		tmux.change_base_dir_session(selection.value.name, dir)
		-- TODO: select session back
	end)

	return true
end

local function results_title()
	local options = require("neovimux.config").options
	return options.tmux.hints and "<c-r><c-n> - rename, <C-d> - change dir, <C-o> - new session" or ""
end

local function previewer()
	local previewers = require("telescope.previewers")
	return previewers.new_termopen_previewer({
		get_command = function(entry)
			if entry.value.name == get_current_tmux_session_name() then
				return { "echo", "Current session" }
			end
			return { "tmux", "capture-pane", "-peJt", entry.value.name }
		end,
		hide_on_startup = true,
	})
end

local function get_telescope_layout_height()
	local options = require("neovimux.config").options
	if options.tmux.layout.height then
		return options.tmux.layout.height
	end
	if options.telescope.layout_strategy == "prompt_center" then
		return 2 + require("neovimux.actions.tmux").tmux_count_sessions() * 2
	end
	if options.telescope.layout_strategy == "prompt_center" then
		return 3 + require("neovimux.actions.tmux").tmux_count_sessions()
	end
end

function M.list_tmux_sessions(opts)
	if package.loaded["telescope"] == nil then
		return M
	end

	local pickers = require("telescope.pickers")
	local config = require("telescope.config").values
	local options = require("neovimux.config").options
	pickers
		.new(opts, {
			finder = tmux_session_finder(),
			sorter = config.generic_sorter(opts),
			previewer = previewer(),
			attach_mappings = keymaps,
			layout_strategy = options.telescope.layout_strategy,
			layout_config = {
				width = options.tmux.layout.width,
				height = get_telescope_layout_height(),
			},
			prompt_title = "tmux sessions",
			-- results_title = "  " .. options.icons.picker.active_session .. " " .. get_current_tmux_session_name(),
			results_title = results_title(),
		})
		:find()
end

return M
